/*jslint plusplus: true*/
/*jslint nomen: true*/
/*global $, jQuery, alert, google, console, outdatedBrowser, IScroll, FastClick, videojs*/

var winw, winh,
  is_touch = (typeof (window.ontouchstart) !== "undefined") || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0);

$(document).ready(function () {
  "use strict";
  FastClick.attach(document.body);
  winw = $(window).width();
  winh = $(window).height();
});
